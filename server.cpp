#include "./DMapServer.h"


int main(int argc, char* argv[]) {
    if (argc >= 2) {
        DMapServer server(stoi(argv[1]));
        server.run();
    } else {
        cout << "You must specify a port" << endl;
    }
    return 0;
}