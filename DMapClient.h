#ifndef DMAPCLIENT_H
#define DMAPCLIENT_H

#include <map>
#include <fstream>
#include <string>
#include <vector>
using namespace std;

#include "TcpSocket/TcpSocketClient.h"
#include "./DMapMsg.h"
#include "./string_utils.h"


struct Server {
    string ip;
    int port;
    int id_map;
};

class DMapClient {
    typedef pair<string, int> si;
    class Iter;
    public:
        DMapClient() {
            load_servers();
            create_map();
        }

        ~DMapClient() {
            erase_map();
        }

        int size() {
            int ret = 0;
            for (int i = 0; i < servers.size(); ++i) {
                TcpSocketClient client;
                client.connect(servers[i].ip, servers[i].port);
                DMapRequest request;
                request.id_map = servers[i].id_map;
                request.action = "size";
                client.write(request.to_string());
                DMapResponse response(client);
                client.close();
                if (response.status == "ok") {
                    ret += stoi(response.detail);
                } else {
                    cout << "Cannot query size of map on server ";
                    cout << servers[i].ip << servers[i].port << endl;
                    cout << response.detail << endl;
                }
            }
            return ret;
        }

        void insert(string key, string val) {
            TcpSocketClient client;
            int idx = hash(key);
            client.connect(servers[idx].ip, servers[idx].port);
            DMapRequest request;
            request.id_map = servers[idx].id_map;
            request.action = "insert";
            request.key = key;
            request.value = val;
            client.write(request.to_string());
            DMapResponse response(client);
            client.close();
            if (response.status != "ok") {
                cout << "Cannot insert key: " + key << endl;
                cout << response.detail << endl;
            }
        }

        void update(string key, string val) {
            TcpSocketClient client;
            int idx = hash(key);
            client.connect(servers[idx].ip, servers[idx].port);
            DMapRequest request;
            request.id_map = servers[idx].id_map;
            request.action = "update";
            request.key = key;
            request.value = val;
            client.write(request.to_string());
            DMapResponse response(client);
            client.close();
            if (response.status != "ok") {
                cout << "Cannot update key: " + key << endl;
                cout << response.detail << endl;
            }
        }

        string get(string key) {
            TcpSocketClient client;
            int idx = hash(key);
            client.connect(servers[idx].ip, servers[idx].port);
            DMapRequest request;
            request.id_map = servers[idx].id_map;
            request.action = "get";
            request.key = key;
            client.write(request.to_string());
            DMapResponse response(client);
            client.close();
            if (response.status != "ok") {
                cout << "Cannot update key: " + key << endl;
                cout << response.detail << endl;
            }
            return response.detail;
        }

        bool exist(string key) {
            TcpSocketClient client;
            int idx = hash(key);
            client.connect(servers[idx].ip, servers[idx].port);
            DMapRequest request;
            request.id_map = servers[idx].id_map;
            request.action = "exist";
            request.key = key;
            client.write(request.to_string());
            DMapResponse response(client);
            client.close();
            if (response.status != "ok") {
                cout << "Cannot query if exist: " + key << endl;
                cout << response.detail << endl;
            }
            return response.detail == "yes";
        }

        void erase(string key) {
            TcpSocketClient client;
            int idx = hash(key);
            client.connect(servers[idx].ip, servers[idx].port);
            DMapRequest request;
            request.id_map = servers[idx].id_map;
            request.action = "erase";
            request.key = key;
            client.write(request.to_string());
            DMapResponse response(client);
            client.close();
            if (response.status != "ok") {
                cout << "Cannot erase key: " + key << endl;
                cout << response.detail << endl;
            }
        }


        Iter begin() {
            return Iter(this, 0, 0);
        }

        Iter end() {
            return Iter(this, servers.size(), 0);
        }
    private:
        vector<Server> servers;

        void load_servers() {
            ifstream f("DMapServers.config");
            Server server;
            while (f >> server.ip >> server.port) {
                servers.push_back(server);
            }
            f.close();
        }

        void create_map() {
            for (int i = 0; i < servers.size(); ++i) {
                TcpSocketClient client;
                client.connect(servers[i].ip, servers[i].port);
                DMapRequest request;
                request.action = "create map";
                client.write(request.to_string());
                DMapResponse response(client);
                client.close();
                if (response.status == "ok") {
                    servers[i].id_map = stoi(response.detail);
                } else {
                    cout << "Cannot create map on server ";
                    cout << servers[i].ip << servers[i].port << endl;
                    cout << response.detail << endl;
                }
            }
        }

        void erase_map() {
            for (int i = 0; i < servers.size(); ++i) {
                TcpSocketClient client;
                client.connect(servers[i].ip, servers[i].port);
                DMapRequest request;
                request.id_map = servers[i].id_map;
                request.action = "erase map";
                client.write(request.to_string());
                DMapResponse response(client);
                client.close();
                if (response.status != "ok") {
                    cout << "Cannot delete map on server ";
                    cout << servers[i].ip << " " << servers[i].port << endl;
                    cout << response.detail << endl;
                }
            }
        }


        int hash(string s) {
            int hash = 0;
            for (int i = 0; i < s.size(); ++i) {
                hash = (hash + s[i]) % servers.size();
            }
            return hash;
        }

        vector<string> get_keys(int idx) {
            if (idx >= servers.size()) {
                return vector<string>();
            }
            TcpSocketClient client;
            client.connect(servers[idx].ip, servers[idx].port);
            DMapRequest request;
            request.id_map = servers[idx].id_map;
            request.action = "keys";
            client.write(request.to_string());
            DMapResponse response(client);
            client.close();
            if (response.status != "ok") {
                cout << "Cannot get keys from server ";
                cout << servers[idx].ip << " " << servers[idx].port << endl;
                cout << response.detail << endl;
            }
            return split(response.detail, ',');
        }

        class Iter {
            public:
                Iter(DMapClient* _map_client,
                         int _server_idx,
                         int _key_idx):
                            map_client(_map_client),
                            server_idx(_server_idx),
                            key_idx(_key_idx) {
                    keys = _map_client->get_keys(server_idx);
                }

                pair<string, string> operator*() {
                    pair<string, string> ret;
                    ret.first = keys[key_idx];
                    ret.second = map_client->get(ret.first);
                    return ret;
                }

                bool operator!=(const Iter& it) {
                    return key_idx != it.key_idx || server_idx != it.server_idx;
                }

                const Iter& operator++() {
                    key_idx++;
                    while (key_idx >= keys.size() &&
                            server_idx < map_client->servers.size()) {
                        server_idx++;
                        key_idx = 0;
                        keys = map_client->get_keys(server_idx);
                    }
                    return *this;
                }

            private:
                int server_idx;
                int key_idx;
                vector<string> keys;
                DMapClient* map_client;
        };
};

#endif