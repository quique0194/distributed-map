#include <iostream>
#include <string>
using namespace std;

#include "./DMapClient.h"

int main() {
    DMapClient map;

    map.insert("kike", "20");
    map.insert("vannet", "21");
    map.insert("diego", "22");
    map.insert("eiego", "23");
    map.insert("piero", "24");

    map.update("kike", "19");

    cout << "EXIST" << endl;
    cout << "jajaj " << map.exist("jajaj") << endl;
    cout << "piero " << map.exist("piero") << endl;

    cout << "SIZE" << endl;
    cout << map.size() << endl;
    map.erase("eiego");
    cout << map.size() << endl;

    cout << "CONTENTS" << endl;
    for (auto x : map) {
        cout << x.first << " " << x.second << endl;
    }

    return 0;
}