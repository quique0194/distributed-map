#ifndef DMAPMSG_H
#define DMAPMSG_H

#include <iostream>
#include <string>
using namespace std;

#include "TcpSocket/TcpSocketConnection.h"
#include "TcpSocket/TcpSocketClient.h"


class DMapRequest {
    public:
        DMapRequest(): id_map(0) {}

        DMapRequest(TcpSocketConnection conn) {
            string first_line = conn.read_line();
            if (first_line != "") {
                id_map = stoi(first_line);
            } else {
                id_map = 0;
            }
            action = conn.read_line();
            key = conn.read_line();
            value = conn.read_line();
        }

        string to_string() {
            string ret;
            ret += ::to_string(id_map) + '\n';
            ret += action + '\n';
            ret += key + '\n';
            ret += value + '\n';
            return ret;
        }

        void info() {
            cout << id_map << " " << action << " ";
            cout << key << " " << value << endl; 
        }

        int id_map;
        string action;
        string key;
        string value;
};

class DMapResponse {
    public:
        DMapResponse(string _status = "",
                     string _detail = ""):
                         status(_status),
                         detail(_detail) {

        }

        DMapResponse(TcpSocketClient client) {
            status = client.read_line();
            detail = client.read_line();
        }

        string to_string() {
            string ret;
            ret += status + '\n';
            ret += detail + '\n';
            return ret;
        }

        string status;
        string detail;
};

#endif