#ifndef DMAPSERVER_H
#define DMAPSERVER_H

#include <map>
#include <string>
using namespace std;

#include "TcpSocket/TcpSocketServer.h"
#include "./DMapMsg.h"
#include "./string_utils.h"


/*
    DMapServer receives Requests in this format:

    id_map
    action: create map, erase map, size, keys, insert, update, erase, exist, get
    key
    value

    DMapServer sends Responses in this format:

    status: ok, error
    detail: description of the error, or extra info if ok
*/

class DMapServer: public TcpSocketServer {
public:
    DMapServer(int port): TcpSocketServer(port), id_cont(1) {

    }

    void handle_conn(TcpSocketConnection conn) {
        DMapRequest request(conn);
        DMapResponse response;

        request.info();

        if (request.action == "create map") {
            response = create_map(request);

        } else if (request.action == "erase map") {
            response = erase_map(request);

        } else if (request.action == "size") {
            response = size(request);

        } else if (request.action == "keys") {
            response = keys(request);

        } else if (request.action == "exist") {
            response = exist(request);

        } else if (request.action == "insert") {
            response = insert(request);

        } else if (request.action == "update") {
            response = update(request);

        } else if (request.action == "erase") {
            response = erase(request);

        } else if (request.action == "get") {
            response = get(request);

        } else {
            response = DMapResponse("error", string("invalid action: ") + request.action);
        }

        conn.write(response.to_string());
        TcpSocketServer::handle_conn(conn);
    }

    // actions

    DMapResponse create_map(DMapRequest request) {
        server_maps[id_cont++] = map<string, string>();
        return DMapResponse("ok", to_string(id_cont-1));
    }

    DMapResponse erase_map(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");
        } else {
            server_maps.erase(request.id_map);
            return DMapResponse("ok");
        }
    }

    DMapResponse size(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");
        } else {
            return DMapResponse("ok", to_string(server_maps[request.id_map].size()));
        }
    }

    DMapResponse keys(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");
        } else {
            vector<string> all_keys;
            for (auto it: server_maps[request.id_map]) {
                all_keys.push_back(it.first);
            }
            return DMapResponse("ok", join(all_keys, ','));
        }
    }

    DMapResponse exist(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");

        } else if (server_maps[request.id_map].count(request.key) > 0) {
            return DMapResponse("ok", "yes");

        } else {
            return DMapResponse("ok", "no");
        }
    }

    DMapResponse insert(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");

        } else if (server_maps[request.id_map].count(request.key) > 0) {
            return DMapResponse("error", "key already exists");
        
        } else {
            server_maps[request.id_map][request.key] = request.value;
            return DMapResponse("ok");
        }
    }

    DMapResponse update(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");

        } else if (server_maps[request.id_map].count(request.key) == 0) {
            return DMapResponse("error", "key does not exist");
        
        } else {
            server_maps[request.id_map][request.key] = request.value;
            return DMapResponse("ok");
        }
    }

    DMapResponse erase(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");

        } else if (server_maps[request.id_map].count(request.key) == 0) {
            return DMapResponse("error", "key does not exist");
        
        } else {
            server_maps[request.id_map].erase(request.key);
            return DMapResponse("ok");
        }
    }

    DMapResponse get(DMapRequest request) {
        if (server_maps.count(request.id_map) == 0) {
            return DMapResponse("error", "unexistent map id");

        } else if (server_maps[request.id_map].count(request.key) == 0) {
            return DMapResponse("error", "key does not exist");
        
        } else {
            return DMapResponse("ok", server_maps[request.id_map][request.key]);
        }
    }


private:
    int id_cont;
    map<int, map<string, string> > server_maps;
};

#endif