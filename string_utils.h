/* Copyright 2014 Jose Carrillo */

#ifndef UTILS_H
#define UTILS_H

#include <vector>
using std::vector;
#include <string>
using std::string;
#include <sstream>
using std::stringstream;
#include <map>
using std::map;


vector<string> split(const string& orig, char delim) {
    vector<string> ret;
    ret.push_back("");
    for (int i = 0; i < orig.size(); ++i) {
        if (orig[i] == delim) {
            ret.push_back("");
        } else {
            ret[ret.size()-1] += orig[i];
        }
    }
    return ret;
}

string join(vector<string> list, char delim) {
    string ret = "";
    for (int i = 0; i < list.size()-1; ++i) {
        ret += list[i] + ',';
    }
    ret += list[list.size()-1];
    return ret;
}

#endif
