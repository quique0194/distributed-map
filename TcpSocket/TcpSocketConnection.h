/* Copyright 2014 Jose Carrillo */

#ifndef TCPSOCKETCONN_H
#define TCPSOCKETCONN_H


#include <string>
using std::string;


#define CHUNK_SIZE 100     // How much chars are read from socket at once


class TcpSocketConnection {
    public:
        explicit TcpSocketConnection(int _connection_fd = 0):
                connection_fd(_connection_fd) {
        }

        string read(int n) {
            /*
                Read n bytes of data
            */
            string ret;
            for (int i = 0; i < n / CHUNK_SIZE; ++i) {
                ret += read_chunk();
            }
            for (int i = 0; i < n % CHUNK_SIZE; ++i) {
                ret += read_byte();
            }
            return ret;
        }

        string read_line() {
            /*
                Support botn '\r\n' and '\n'
                Return val doesn't include '\r\n' nor '\n'
            */
            string ret;
            char next = read_byte();
            while (next != '\n') {
                ret += next;
                next = read_byte();
            }
            if (ret.back() == '\r') {
                ret.erase(ret.end()-1, ret.end());
            }
            return ret;
        }

        char read_byte() {
            char ret;
            ::read(connection_fd, &ret, 1);
            return ret;
        }

        void write(const string& msg) {
            ::write(connection_fd, msg.c_str(), msg.size());
        }

        void close() {
            shutdown(connection_fd, SHUT_RDWR);
            ::close(connection_fd);
            connection_fd = 0;
        }

        bool is_connected() {
            return connection_fd != 0;
        }

    private:
        int connection_fd;

        string read_chunk() {
            char buf[CHUNK_SIZE+1];
            int n = ::read(connection_fd, buf, CHUNK_SIZE);
            buf[n] = 0;    // null terminating char
            return string(buf);
        }
};

#endif
