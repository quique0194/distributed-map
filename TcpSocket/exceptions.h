/* Copyright 2014 Jose Carrillo */

#ifndef EXCEPTIONS_H
#define EXCEPTIONS_H

#include <exception>

using std::exception;


class CreateSocketException: public exception {
    virtual const char* what() const throw() {
        return "Cannot create socket";
    }
};


class InvalidIpException: public exception {
    virtual const char* what() const throw() {
        return "Invalid IP address";
    }
};


class ConnectionException: public exception {
    virtual const char* what() const throw() {
        return "Connection failed";
    }
};

class BindingException: public exception {
    virtual const char* what() const throw() {
        return "Binding failed";
    }
};

class ListenException: public exception {
    virtual const char* what() const throw() {
        return "Listen failed";
    }
};

class AcceptConnectionException: public exception {
    virtual const char* what() const throw() {
        return "Accept connection failed";
    }
};

#endif
