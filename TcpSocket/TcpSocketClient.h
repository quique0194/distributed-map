/* Copyright 2014 Jose Carrillo */

#ifndef TCPSOCKETCLIENT_H
#define TCPSOCKETCLIENT_H

#include <sys/types.h>      // socket
#include <sys/socket.h>     // socket
#include <arpa/inet.h>      // htons, inet_pton

#include <string.h>         // memset
#include <unistd.h>         // read, write

#include <string>
using std::string;

#include "./exceptions.h"


class TcpSocketClient {
    public:
        TcpSocketClient() {
            socket_fd = socket(AF_INET, SOCK_STREAM, IPPROTO_TCP);

            if (socket_fd == -1)
                throw CreateSocketException();
        }

        void connect(const string& ip, int port) {
            memset(&stSockAddr, 0, sizeof(struct sockaddr_in));
            stSockAddr.sin_family = AF_INET;
            stSockAddr.sin_port = htons(port);
            if (0 == inet_pton(AF_INET, ip.c_str(), &stSockAddr.sin_addr))
                throw InvalidIpException();

            if (-1 == ::connect(socket_fd,
                                (const struct sockaddr *)&stSockAddr,
                                sizeof(struct sockaddr_in)))
                throw ConnectionException();
        }

        void close() {
            shutdown(socket_fd, SHUT_RDWR);
            ::close(socket_fd);
        }

        void write(const string& msg) {
            ::write(socket_fd, msg.c_str(), msg.size()+1);
        }

        string read_line() {
            /*
                Support botn '\r\n' and '\n'
                Return val doesn't include '\r\n' nor '\n'
            */
            string ret;
            char next = read_byte();
            while (next != '\n') {
                ret += next;
                next = read_byte();
            }
            if (ret.back() == '\r') {
                ret.erase(ret.end()-1, ret.end());
            }
            return ret;
        }

        char read_byte() {
            char ret;
            ::read(socket_fd, &ret, 1);
            return ret;
        }

    private:
        struct sockaddr_in stSockAddr;
        int socket_fd;
};

#endif
